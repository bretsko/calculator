//
//  ViewController.m
//  Calculator
//
//  Created by Admin on 10/5/15.
//  Copyright (c) 2015 Softserve. All rights reserved.
//

#import "ViewController.h"
#import <math.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *InputField;
@property (strong,nonatomic) NSDecimalNumber * FirstOperand;
@property (strong,nonatomic) NSDecimalNumber * SecondOperand;
@property (strong,nonatomic) NSDecimalNumber * Result;
@property(strong,nonatomic)  NSNumberFormatter *formatter;
@property (strong,nonatomic) NSString * CurrentOperation;
@property (strong,nonatomic) NSString * positiveFromInputField;
@property (strong,nonatomic) NSMutableString * helperString;

@property (assign, nonatomic) BOOL inputFieldHasNumber;
@property (assign, nonatomic) BOOL firstOperandEntered;
@property (assign, nonatomic) BOOL secondOperandEntered;
@property (assign, nonatomic) BOOL operationEntered;
@property (assign, nonatomic) BOOL isNegative;
@property (assign, nonatomic) BOOL dotPresent;

//specialNumberPresent is used to prevent appending PI, e after PI or e has been entered
@property (assign, nonatomic) BOOL specialNumberPresent;

//additionalOperandPresent is used to save e, PI to memory
@property (assign, nonatomic) BOOL additionalOperandPresent;

@property (nonatomic) NSInteger numberOfDecimalPlaces;
@property (nonatomic) NSInteger numberOfSeparatorsPresent;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.InputField.text = @"0";
    [self.formatter setLocale:[NSLocale currentLocale]];
    [self.formatter setNumberStyle:NSNumberFormatterDecimalStyle];
}

- (BOOL) inputFieldHasNumber {
// but maybe 0
if (![self.InputField.text isEqualToString:(@"-")] && [self.InputField.text length] > 0)
  {
   self.inputFieldHasNumber = YES;
   return YES;
  }
  else return NO;
}

  //self.InputField.text = number.currentTitle;

#pragma mark - numberEntered

- (IBAction)numberEntered:(UIButton *)number {
//TODO check if empty is possible (at the start?)

if ([number.currentTitle isEqualToString:(@"+ / -")])
  {
    //check  if there is no 0
    if (((self.operationEntered)  &&
    (!self.secondOperandEntered)) ||
    [self.InputField.text isEqualToString:(@"0")]  ||
    [self.InputField.text isEqualToString:(@"0.0")] ||
    [self.InputField.text isEqualToString:(@"")])
    {
    self.InputField.text = @"-";
    self.isNegative = YES;
     
    // if (!self.secondOperandEntered)
    //  {
    //switching second operand into append mode
    //TODO can't use bare minus as an operand, check for minus in operation and isNegative
    self.secondOperandEntered = YES;
    }

    else if (!self.firstOperandEntered)
    {
    //switching first operand into append mode
    self.firstOperandEntered = YES;
    }

  else if (![self checkInputForNegative])
    {
     //Alternative to BOOL register:
     //NSRange range = [self.InputField.text rangeOfString:@"-"];
     //if(range.location == NSNotFound){
     //self.InputField.text= [@"-" stringByAppendingString:self.InputField.text];}

  //    NSDecimalNumber * num = [ [self stringToDecimal:self.InputField.text] decimalNumberByMultiplyingBy:
  //         [NSDecimalNumber decimalNumberWithMantissa: 1
  //                                           exponent: 0
  //                                         isNegative: YES]];
  //    self.InputField.text = [num stringValue];
     self.InputField.text = [NSString stringWithFormat:@"-%@", self.InputField.text];
    }
  
  else if ([self checkInputForNegative])
    {
      self.InputField.text = [self positiveFromInputField];
      
    //    NSRange range = NSMakeRange (1, self.InputField.text.length - 1);
   //  self.InputField.text = NSStringFromRange(range);
    
    //Alternative
    
     //unsigned long num =[[self stringToDecimal:self.InputField.text]unsignedLongValue];
     //self.InputField.text = [NSString stringWithFormat:@"%lu",num];
    }
  }
    
//if the button is not "+ / -"
else if (self.operationEntered)
  {
     if (self.secondOperandEntered)
       {
       [self getInputToScreen:number];
       }
      
     else if (!self.secondOperandEntered)
       {
       self.InputField.text = number.currentTitle;
       //switching second operand into append mode
       self.secondOperandEntered = YES;
       }
  }

else if (!self.operationEntered)
  {
  if (!self.firstOperandEntered)
    {
  //  if([self inputFieldHasNumber])
    if (!(self.isNegative) && ![self.InputField.text isEqualToString:(@"-")])
       //checking just in case the lock is not on
       {
       //No operands - Entering first operand
       [self getInputToScreen:number];
       // changed    self.InputField.text = number.currentTitle;
       self.firstOperandEntered = YES;
       }
    }
    
  //Still entering first operand
  else if (self.firstOperandEntered)
    {
    [self getInputToScreen:number];
    }
  }
}


// TODO 2 types of operations - immediate - sqrt, pow,
#pragma mark - operationEntered

- (IBAction)operationEntered:(UIButton *)operation {

if([self inputFieldHasNumber])
//WAS if([self.InputField.text length] > 0 && ![self.InputField.text isEqualToString:(@"-")])
    //TODO can't use bare minus as an operand, check for minus in operation and isNegative
  {
  if(!self.secondOperandEntered)
    {
    self.FirstOperand =  [NSDecimalNumber decimalNumberWithString:self.InputField.text];
    self.firstOperandEntered = YES;

    // self.secondOperandEntered == NO;

    self.CurrentOperation = operation.currentTitle;
    self.operationEntered = YES;
    //resetting dot for the next operand
    self.dotPresent = NO;
    }

  else if(self.secondOperandEntered)
    {
    [self evaluateAndShowResult];
      
    //save the result in case it will be used for the next operation
    self.FirstOperand =  [NSDecimalNumber decimalNumberWithString:self.InputField.text];
    self.firstOperandEntered = YES;

    self.CurrentOperation = operation.currentTitle;
    self.operationEntered = YES;
    //resetting dot for the next operand
    self.dotPresent = NO;
    }
  }
}


#pragma mark - getInputToScreen (input validation / sanitazation)

- (void) getInputToScreen: (UIButton *) number {

//getting 0 for 0
//WAS if ([self.InputField.text isEqualToString:(@"0")] && [number.currentTitle isEqualToString:(@"0")])
if (![self inputFieldHasNumber] && [number.currentTitle isEqualToString:(@"0")])
    {
    self.InputField.text = @"0";
    }
  
//TODO output symbols but mean numbers, or parse later symbols to get numbers
//round them?
    //TODO  e
  
//checking the Input field and getting number for 0 and empty
else if ( ([self.InputField.text isEqualToString:@"0"] &&
            ![number.currentTitle isEqualToString:@"0"]) ||
            [self.InputField.text isEqualToString:@"0"] ||
            [self.InputField.text isEqualToString:@""] ||
            self.specialNumberPresent ||
            self.InputField.text.length == 0 )
  {
  self.InputField.text = number.currentTitle;
  }

// if separator needed append from back, else append in front
     
  // ADD SEPARATOR if needed
#pragma mark - processSeparator, Uncomment it

else{
[self countDecimalPlacesBeforeComma];
[self processSeparator:number];

}

    //else if ([self.CurrentOperation isEqualToString:@"e"])
  //  {
  //  self.InputField.text = [NSString stringWithFormat:@"%@",@M_E];
  //  //TODO implement output e for string calculator
  //  // self.InputField.text = @"e";
  //  self.specialNumberPresent = YES;
  //  }
  //
  //    //TODO PI
  //else if ([self.CurrentOperation isEqualToString:@"PI"])
  //  {
  //  self.InputField.text = [NSString stringWithFormat:@"%@",@M_PI];
  //self.InputField.text = @"PI";

  //NSString *stringwithpi = [@"anystring" stringByAppendingString:@"\u03C0"];
  //NSLog(@"TEST  : %@", stringwithpi);
  //Basically, use unicode "\u03C0" to append PI
  //
  //Specifically, for cocos2d:
  //NSString *labelpi = [@"lifeof" stringByAppendingString:@"\u03C0"];
  //CCLabelTTF *label = [CCLabelTTF labelWithString:labelpi fontName:@"Marker Felt" fontSize:64];


  // if e or PI is present can't add numbers to them just operations,
  // operand is present -
             
             
  // TODO  get PI symbol, but mean M_PI -
  //either sum with first operand or save to sum in final operation
           
  //self.specialNumberPresent = YES;
           
  //}

}

#pragma mark - processSeparator

- (void) processSeparator: (UIButton *) number {
  
    
// append Separator    
if (self.numberOfDecimalPlaces > 2 || [self countDecimalPlacesBeforeComma] > 2)
  {
    if (self.numberOfDecimalPlaces == 3 ||
        self.numberOfDecimalPlaces == 6 ||
        self.numberOfDecimalPlaces == 9 )
    {
    // if needed append Separator before appending cself.numberOfSeparatorsPresent = 1;urrent number
    // OR use NSScanner to scan

//SWITCH not working - can't get to check //  if (self.numberOfDecimalPlaces % 3 == 0)
//    switch (self.numberOfDecimalPlaces / 3)
//      {
//       // TODO - what is the maximum number of digits that can be seen in the Input field
//       case 1:

if (self.numberOfDecimalPlaces == 3){
      
//              if (self.isNegative)
//                {
//                 //Input is clean, no need to check: if ((!self.dotPresent)||((self.dotPresent)&&(self.firstOperandEntered)   ))
//                  //appending current number
//                 self.helperString = [NSMutableString stringWithFormat:@"-%@\'%@", number.currentTitle,[self positiveFromInputField]];
//                  self.InputField.text = self.helperString;
//                self.numberOfSeparatorsPresent = 1;
//                }
//                //if not negative
//              
//              else
//                {
                    //append one number in front after the first 3 numbers entered
                self.helperString = [NSMutableString stringWithFormat:@"%@\'%@", number.currentTitle,self.InputField.text];
                self.InputField.text = self.helperString;
              
                self.numberOfSeparatorsPresent = 1;
//                break;
                }
              
              // Todo - must be here - self.numberOfSeparatorsPresent = 1;
else if (self.numberOfDecimalPlaces == 6){
//       case 2:
//              if (self.isNegative)
//                {
//                 //Input is clean, no need to check: if ((!self.dotPresent)||((self.dotPresent)&&(self.firstOperandEntered)   ))
//                  //appending current number
//                  self.helperString = [NSMutableString stringWithFormat:@"-%@\'%@", number.currentTitle,[self positiveFromInputField]];
//                  self.InputField.text = self.helperString;
//                }
//                //if not negative
//              else
//                {
                self.helperString = [NSMutableString stringWithFormat:@"%@\'%@", number.currentTitle,self.InputField.text];
                self.InputField.text = self.helperString;
              self.numberOfSeparatorsPresent = 2;
               }
//              break;

else if (self.numberOfDecimalPlaces == 9){

 //       case 3:
//              if (self.isNegative)
//                {
//                 //Input is clean, no need to check: if ((!self.dotPresent)||((self.dotPresent)&&(self.firstOperandEntered)   ))
//                  //appending current number
//                 self.helperString = [NSMutableString stringWithFormat:@"-%@\'%@", number.currentTitle,[self positiveFromInputField]];
//                  self.InputField.text = self.helperString;
//                }
//                //if not negative
//              else
//                {
                self.helperString = [NSMutableString stringWithFormat:@"%@\'%@", number.currentTitle,self.InputField.text];
                self.InputField.text = self.helperString;
                self.numberOfSeparatorsPresent = 3;
                }

//              break;
      //default:
               // self.numberOfSeparatorsPresent = 0;
    }
    
 // append from the front       
else if (1)
       {
    // !self.numberOfDecimalPlaces % 3 == 0
    // no need to add Separator
   //  move Separator(s) to the right position(s) before appending number

 //     switch (self.numberOfSeparatorsPresent)
  //      {
            
// impossible       case 0:
//                  break;
            //case 1:
//               if (self.isNegative)
//                 {
//                 //Input is clean, no need to check: if ((!self.dotPresent)||((self.dotPresent)&&(self.firstOperandEntered)   ))
//                  //appending current number
//                 self.helperString = [NSMutableString stringWithFormat:@"-%@%@", number.currentTitle,[self positiveFromInputField]];
//                 self.InputField.text = self.helperString;
//                 }
//              //if not negative
//              else
//                {
        self.helperString = [NSMutableString stringWithFormat:@"%@%@", number.currentTitle,self.InputField.text];
        self.InputField.text = self.helperString;
               //break;
//                }
      // case 2:

      // case 3:

       // default:

               //break;
           
      }
else {
  // PROBLEM - appending number but from the wrong side
  self.InputField.text = [self.InputField.text stringByAppendingString:number.currentTitle];

}
  }
}

- (NSInteger) countDecimalPlacesBeforeComma {
//Alternative 2
NSScanner *scanner = [NSScanner scannerWithString:self.InputField.text];
    
NSString *upperSeparatorString = @"\'";
NSString *lowerSeparatorString = @".";
    
NSString * container, * container1;
//NSString  * container1;

NSCharacterSet * upperSeparatorCharacterset = [NSCharacterSet characterSetWithCharactersInString:upperSeparatorString];

NSCharacterSet * lowerSeparatorCharacterset = [NSCharacterSet characterSetWithCharactersInString:lowerSeparatorString];
    
//1  PROBLEM, not trimming
//container = [self.InputField.text stringByTrimmingCharactersInSet:upperSeparatorCharacterset];

//first get position of range of comma


//2
[scanner scanUpToCharactersFromSet:lowerSeparatorCharacterset intoString:&container];
//OR [scanner scanUpToString:lowerSeparatorString intoString:&container1];

scanner = [NSScanner scannerWithString:container];
[scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet]
                    intoString:&container1];
  
// can use container1 as a helper string to calculate further



self.numberOfDecimalPlaces = container1.length;
  
//Alternative 3 - exaperimenting
// [scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet]
//                        intoString:&container];
    
//        while ([theScanner isAtEnd] == NO) {
//        [theScanner scanFloat:&aFloat];

// NSInteger anInteger;
// [scanner scanInteger:&anInteger];
  

    
    //self.numberOfDecimalPlaces = self.InputField.text.length - [self.InputField.text rangeOfString:@"."].location - 1;
    
    
    //    NSString *resultString;
    //    while ([scanner scanUpToCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet] intoString:&resultString]) {
    //        NSLog(@"Found string: %@",resultString);
    
    
//Alternative 2
// floor decimal input to get number of digits, double is always positive
//double doubleInput = [self.InputField.text doubleValue];
                                                              
//self.numberOfDecimalPlaces = [[NSString stringWithFormat:@"%0.f", floor(doubleInput)] length];
                                                              
return self.numberOfDecimalPlaces;

                                                              
//    static int i = 0;
//    // set it to skip non-numeric characters

// SCAN AND SKIP ALL NON-DECIMAL
//    [scanner setCharactersToBeSkipped:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
//    
//    while ([scanner scanInt:&i])
//    {
//        ++i;
//    }
//    self.countForUpperSeparator = i;

    

// can be used for STRING CALCULATOR
    // reset the scanner to skip numeric characters
//    [scanner setScanLocation:0];
//    [scanner setCharactersToBeSkipped:[NSCharacterSet decimalDigitCharacterSet]];
//    }
    
 
//    NSScanner* scan = [NSScanner scannerWithString:self.InputField.text];
//    int val;
//    return [scan scanInt:&val] && [scan isAtEnd];
    
//    if ([self.InputField.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
//        NSLog(@"This is not a positive integer");
//    }
    
    
    //OR register and not count   -  HOW??
    //++numberOfDigits;
}


#pragma mark - equalsEntered

- (IBAction)equalsEntered:(UIButton *)button {
  // TODO check if additional operand is present and take it
  
  //checking if isNegative was the last operation
  // in this case, self.operationEntered should be NO
  
  //next isNegative takes the result on the screen as first operand
  
  // need operation and input
if(          !self.operationEntered &&
                 self.SecondOperand &&
              self.CurrentOperation &&
  [self.InputField.text length] > 0 &&
  ![self.InputField.text isEqualToString:(@"-")] )
    {
    self.FirstOperand =  [NSDecimalNumber decimalNumberWithString:self.InputField.text];
    self.firstOperandEntered = YES;
    // reusing the Second Operand and Operation from the last operation
    self.secondOperandEntered = YES;
    self.operationEntered = YES;
    [self evaluateAndShowResult];
    }

else if([self inputFieldHasNumber])
    {
    self.SecondOperand =  [NSDecimalNumber decimalNumberWithString:self.InputField.text];
    self.secondOperandEntered = YES;
    [self evaluateAndShowResult];
    }
}



-(void)evaluateAndShowResult {
//self.SecondOperand = [self getDecimalInput];
    
if ([self.CurrentOperation isEqualToString:@"+"])
  {
  self.Result = [self.FirstOperand decimalNumberByAdding:self.SecondOperand];
  [self showResult];
  }
  
else if ([self.CurrentOperation isEqualToString:@"-"])
  {
  self.Result = [self.FirstOperand decimalNumberBySubtracting:self.SecondOperand];
  [self showResult];
  }

else if ([self.CurrentOperation isEqualToString:@"*"])
  {
  self.Result = [self.FirstOperand decimalNumberByMultiplyingBy:self.SecondOperand];
  [self showResult];
  }


else if ([self.CurrentOperation isEqualToString:@"^2"])
  {
  self.Result = [self.FirstOperand decimalNumberByRaisingToPower:2];
  [self showResult];
  }

else if ([self.CurrentOperation isEqualToString:@"^n"])
  {
    //double pow (double x, double y) -- Compute x raised to the power y.
    //double pow ( double, double ) – power of
    //NSLog(@”%.f”, pow(3,2) ); //result 9
    //NSLog(@”%.f”, pow(3,3) ); //result 27

    if (self.SecondOperand > 0)
      {
       self.Result = [self.FirstOperand decimalNumberByRaisingToPower:[self.SecondOperand unsignedIntegerValue]];
       [self showResult];
      }
    else
     {
     self.InputField.text = @"Please enter positive number";
     }
  }
  
//TODO check
    //
    //dividingbyzero returns    HUGE_VAL -- positive infinity  ??
    //
    
else if ([self.CurrentOperation isEqualToString:@"/"])
  {
  @try
    {
    self.Result  = [self.FirstOperand decimalNumberByDividingBy:self.SecondOperand];
       
    //             {
    //
    //                  if (self.SecondOperand == [NSDecimalNumber zero])
    //                  {
    //                    self.InputField.text = @"Cannot divide by zero";
    //                    //red or other colour text
    //                    [self resetAll];
    //                   }
    //                  
    //                  else if (self.SecondOperand > 0 || self.SecondOperand < 0 )
    //                  {
    //                  self.Result  = [self.FirstOperand decimalNumberByDividingBy:self.SecondOperand];
    //                  [self showResult];
    //                  }
    //            }
   }

  @catch (NSException *exc)
    {
    self.InputField.text = @"Cannot divide by zero";
    }
  }
  
  
#pragma mark - TODO implement immediate operations
else if ([self.CurrentOperation isEqualToString:@"sqrt"])
    {
        
        //double sqrt(double x) -- Compute the square root of x.
        //double  sqrt( double ) – square root
        //NSLog(@”%.f”, sqrt(16) ); //result 4
        //NSLog(@”%.f”, sqrt(81) ); //result 9
      if (self.FirstOperand > 0)
        {
//          long double SquareRoot = sqrtl([self.FirstOperand longLongValue]);
//          self.InputField.text = [NSString stringWithFormat:@"%Lf", SquareRoot];
//          [self resetAll];
        }
      else
        {
        self.InputField.text = @"Please enter positive number";
         return;
        }
     }
    
    // cubic root
   else if ([self.CurrentOperation isEqualToString:@"cbrt"])
   {
       if (self.FirstOperand > 0)
       {
           //cbrt()
           
//           SquareRoot = sqrtl([self.FirstOperand longLongValue]);
//           self.InputField.text = [NSString stringWithFormat:@"%Lf", SquareRoot];
//           [self resetAll];
       }
       else
       {
//           self.InputField.text = @"Please enter positive number";
//           return;
       }
   }
    
 if ([self.CurrentOperation isEqualToString:@"|x|"])
   {
       
       //double fabs (double x ) -- Compute absolute value of x.
       
       if (self.FirstOperand > 0)
       {
//           SquareRoot = sqrtl([self.FirstOperand longLongValue]);
//           self.InputField.text = [NSString stringWithFormat:@"%Lf", SquareRoot];
//           [self resetAll];
       }
   }
  
  
  
   else if ([self.CurrentOperation isEqualToString:@"exp"])
   {
       //    double exp(double x -- Compute exponential of x
   }
 
    
   else if ([self.CurrentOperation isEqualToString:@"log"])
   {
         //double log(double x) -- Compute log(x).
   }
    
   else if ([self.CurrentOperation isEqualToString:@"log10"])
   {
     //double log10 (double x ) -- Compute log to the base 10 of x.
   }
    
    
   else if ([self.CurrentOperation isEqualToString:@"ln"])
   {
   }
   
   else if ([self.CurrentOperation isEqualToString:@"sin"])
   {
       //double sin(double x) -- Compute sine of angle in radians.
   }
  
   else if ([self.CurrentOperation isEqualToString:@"asin"])
   {
         //double asin(double x) -- Compute arc sine of x.

   }
    
   else if ([self.CurrentOperation isEqualToString:@"acos"])
   {
         //double acos(double x) -- Compute arc cosine of x.
   }
    
   else if ([self.CurrentOperation isEqualToString:@"cos"])
   {
       //double cos(double x) -- Compute cosine of angle in radians.
     
   }
    
   else if ([self.CurrentOperation isEqualToString:@"tan"])
   {
        //double tan(double x) -- Compute tangent of angle in radians.
   }
   
   else if ([self.CurrentOperation isEqualToString:@"atan"])
   {
           //double atan(double x) -- Compute arc tangent of x.
   }
   
   else if ([self.CurrentOperation isEqualToString:@"mod"])
   {
       // fmod (modulus % function), remainder
   }
    

    //double ceil ( double ) – if the argument has any decimal part, returns the next bigger integer
    //NSLog(@”res: %.f”, ceil(3.000000000001)); //result 4
    //NSLog(@”res: %.f”, ceil(3.00)); //result 3
    //
  
  
    //double round ( double ) – rounds the argument
    //NSLog(@”res: %.f”, round(3.5)); //result 4
    //NSLog(@”res: %.f”, round(3.46)); //result 3
    //NSLog(@”res: %.f”, round(-3.5)); //NB: this one returns -4
    //
    
}



//TODO CHECK
- (IBAction)dotEntered:(UIButton *)button {

  if ( ((!self.firstOperandEntered) &&
           (!self.operationEntered)) ||
           ((self.operationEntered) &&
           (!self.secondOperandEntered) ))
  {
      self.InputField.text = @"0.";
      self.dotPresent = YES;
      
        if ((!self.operationEntered)&& (!self.firstOperandEntered))
        {
        self.firstOperandEntered = YES;
        }
        
        else if ((self.operationEntered)&& (!self.secondOperandEntered))
        {
        self.secondOperandEntered = YES;
        }
   }
      
 else if(!self.dotPresent)
   {
     //if([self.InputField.text length] == 0)
     //
       //{
        //if number is present add dot to it
        if([self.InputField.text length] > 0)
        {
         //Alternative to BOOL register:
         //NSRange range = [self.InputField.text rangeOfString:@"."];
         //if(range.location == NSNotFound){
         //self.InputField.text= [self.InputField.text stringByAppendingString:@"."];}

            self.InputField.text = [self.InputField.text stringByAppendingString:@"."];
            self.dotPresent = YES;
        }
        //assume 0 if number is not entered yet, or if the state has been reset
        else if( [self.InputField.text length] == 0)
        {
            self.InputField.text = @"0.";
            self.dotPresent = YES;
        }
    }
    
}



- (BOOL)checkInputForNegative {
  if ([[self.InputField.text substringFromIndex:0]isEqualToString:(@"-")])
    {
    self.isNegative = YES;
    return YES;
    }
  else
    {
    self.isNegative = NO;
    return NO;
    }
}

- (NSString *) positiveFromInputField {
NSRange range = NSMakeRange (1, self.InputField.text.length - 1);
self.positiveFromInputField = NSStringFromRange(range);
return self.positiveFromInputField;
}

- (NSDecimalNumber *) stringToDecimal: (NSString *) string {
    return [NSDecimalNumber decimalNumberWithString:string];
}

-(void)showResult {
//TODO - how many digits should it show
  if ([[self.Result stringValue] length] > 9)
    {
    //scientific exponent
    self.InputField.text = [NSString stringWithFormat:@"%g", [self.Result doubleValue]];
    }
  else
    {
    self.InputField.text = [self.Result stringValue];
    [self resetAll];
    }
}

- (void)resetAll {
    self.firstOperandEntered = NO;
    self.operationEntered = NO;
    // need to save second operand for anotherisNegativesOperation
    self.secondOperandEntered = NO;
    self.dotPresent = NO;
    self.numberOfDecimalPlaces = 0;
    self.numberOfSeparatorsPresent = 0;
    self.specialNumberPresent = NO;
}

- (IBAction)clearAll {
    [self clearScreen];
    [self resetAll];
    //WHY?
    [super viewDidLoad];

}
 
- (IBAction)removeNumber {
    if([self.InputField.text length] > 0){
    self.InputField.text = [self.InputField.text substringToIndex:[self.InputField.text length]-1];
    }
}

- (void) clearScreen {
      self.InputField.text =@"0";

}

@end


