//
//  ViewController.m
//  Calculator
//
//  Created by Admin on 10/5/15.
//  Copyright (c) 2015 Softserve. All rights reserved.
//

#import "ViewController.h"
#import <math.h>

@interface ViewController ()

// experiment #define WIDTH(view) view.frame.size.width

#pragma mark IMPLEMENT LOGIC
// RULE: firstOperandEntered OR secondOperandEntered mean valid numbers are
// accepted

// DURING INITIAL INPUT (!self.operationEntered ??)
// Invalid numbers:

// a) -

// b) 0 (self.inputHasNull), 0 can be entered as a valid input to certain
// operations (not sqrt, cbrt, etc.). When null is entered - set
// self.inputhasnull = NO

// c) -0. is considered 0  - self.dotEntered, self.inputField.length < 4
//  - set self.inputhasnull = NO

// d) 0. is considered 0 - self.dotEntered, self.inputField.length < 3
//  - set self.inputhasnull = NO

// OPERAND INPUT VALIDATION
// first check if self.inputField.length < 4 => check for all 4 cases
// else (no need to check 4 cases) => the input number can be accepted as an
// operand -

// RULE: sign logic is implemented through isNegative, no need to check minus in
// initial input mode

// AFTER RESULT HAS BEEN RECEIVED (!self.firstOperandEntered ??, what about
// saving the result for the next operation - shall we use additional operand
// here => check for it then before equalsEntered, operationEntered,
// evaluateImmediately) only Results(new numbers) need to be checked for null
// (when self.inputField.length == 1)

//#warning my warning

@property(weak, nonatomic) IBOutlet UILabel *InputField;

// OPERATION LOGIC (currently also used to realize append mode)
@property(strong, nonatomic) NSDecimalNumber *firstOperand;
@property(strong, nonatomic) NSDecimalNumber *secondOperand;
// the result of the last operation is stored here
@property(strong, nonatomic) NSDecimalNumber *operationResult;

// not immediate operations  (like sqrt, pow, abs, etc.)
@property(strong, nonatomic) NSString *currentOperation;

@property(strong, nonatomic) NSDecimalNumber *savedOperand;
@property(strong, nonatomic) NSString *savedOperation;
// only a number (including a null) can be here,
#pragma mark FUTURE : IMPLEMENT append mode for minus instead of using operation logic

// CURRENTLY minus is also possible, need to check for it before every
// evaluation (equals operation, immediate operation, operation with a previous
// result, ) because it can't be considered as an operand

@property(assign, nonatomic) BOOL firstOperandEntered;
// then
@property(assign, nonatomic) BOOL operationEntered;
// then
@property(assign, nonatomic) BOOL secondOperandEntered;

// states (characteristics) of input field
@property(assign, nonatomic) BOOL isNegative;
@property(assign, nonatomic) BOOL dotPresent;

// default state is "null" but this 0 can't be used as an operand, it's
// considered as an empty string
#pragma mark FUTURE : IMPLEMENT defaultState 0(rename from inputFieldHasNull), check that when inputFieldHasNull it is never used as an operand null)
@property(assign, nonatomic) BOOL inputFieldHasNull;
@property(assign, nonatomic) BOOL inputFieldHasAMinus;
@property(assign, nonatomic) BOOL inDefaultState;

@property(assign, nonatomic) BOOL doubleResultPresent;

// specialNumberPresent is used to prevent appending PI, e after PI or e has
// been entered and to save e, PI to memory and output them as
// symbols
@property(assign, nonatomic) BOOL specialNumberPresent;

#pragma mark FUTURE : output of special numbers(e, PI, etc.)
@property(assign, nonatomic) BOOL savedOperandPresent;
@property(assign, nonatomic) BOOL savedOperationPresent;

#pragma mark CHECK : if const, weak or assign(static) can be used here
// often used literals
//@property (strong, nonatomic) NSString *emptyString;
@property(strong, nonatomic) NSString *nullString;
@property(strong, nonatomic) NSString *nullDotString;
@property(strong, nonatomic) NSString *minusString;
@property(nonatomic) NSRange rangeFirstNumber;
@property(nonatomic) NSRange rangeSecondNumber;

// FORMATTING
// a helper string without grouping separators, it can be used for calculations
// as is
@property(strong, nonatomic) NSMutableString *stringWithoutSeparators;
@property(strong, nonatomic) NSString *positiveFromInputField;
@property(nonatomic) double doubleResult;

#pragma mark CHECK : if need so many formatters
@property(strong, nonatomic) NSNumberFormatter *formatter;
@property(strong, nonatomic) NSNumberFormatter *formatterResult;
@property(strong, nonatomic) NSNumberFormatter *formatterInput;
@property(strong, nonatomic) NSNumberFormatter *formatterScientific;

#pragma mark CHECK : if we need the rest
@property(nonatomic) NSLocale *locale;
@property(nonatomic) NSString *decimalSeparator;
@property(nonatomic) NSString *groupingSeparator;
@property(nonatomic) NSDecimalNumberHandler *handler;

@property (nonatomic)  UIColor * currentInputFieldTextColor ;
@property (nonatomic) UIColor * currentInputFieldbackgroundColor;

// used this for concatenation of decimal numbers before and after a
// DecimalSeparator, for parsing
// @property (strong, nonatomic) NSMutableString *prefix;
// @property (strong, nonatomic) NSMutableString *suffix;

// these can be used to implement default 0.0
//@property (assign, nonatomic) BOOL inputFieldHasNullNull;
//@property (assign, nonatomic) BOOL inputFieldHasMinusNullNull;
//@property (strong,nonatomic) NSString * minusNulNullString;
@end
#pragma mark -
@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // self.emptyString = @"";
  self.minusString = @"-";
  self.nullString = @"0";
// self.nullDotString = @"0.";
#pragma mark CHECK : if we need empty here or null
  self.InputField.text = self.nullString;
  self.inDefaultState = YES;
  self.stringWithoutSeparators = [NSMutableString new];
  [self.stringWithoutSeparators setString:self.nullString];

// should work by deafult self.dotPresent = NO;

#pragma mark CHECK : formatters

  self.locale = [NSLocale currentLocale];
  self.decimalSeparator = [self.locale objectForKey:NSLocaleDecimalSeparator];

  // self.formatter = [NSNumberFormatter new];
  self.formatterInput = [NSNumberFormatter new];
  self.formatterResult = [NSNumberFormatter new];
  self.formatterScientific = [NSNumberFormatter new];

  [self.formatterInput setLocale:self.locale];
  [self.formatterInput setNumberStyle:NSNumberFormatterDecimalStyle];

  [self.formatterInput setAlwaysShowsDecimalSeparator:YES];
  [self.formatterResult setAlwaysShowsDecimalSeparator:YES];

  // FIXME: NOT WOKING
  //[self.formatterInput setMinimumFractionDigits:10];
  //[self.formatterInput setMinimumSignificantDigits:10];
  //[self.formatterInput setMinimumIntegerDigits:10];

  // self.formatterInput.minimumFractionDigits = 10;
  // self.formatterInput.minimumIntegerDigits = 10;
  // self.formatterInput.minimumSignificantDigits = 10;

  //[self.formatterInput setMaximumFractionDigits:8];
  //[self.formatterInput setMaximumSignificantDigits:9];
  //[self.formatterInput setAccessibilityHint:@"Enter a number"];
  // self.formatterInput.usesGroupingSeparator = YES;
  [self.formatterInput setUsesGroupingSeparator:YES];
  //[self.formatterResult setLocale: self.locale];
  [self.formatterResult setNumberStyle:NSNumberFormatterDecimalStyle];

  //[self.formatterResult setMaximumFractionDigits:8];
  //[self.formatterResult setMaximumSignificantDigits:9];
  //[self.formatterResult setAccessibilityHint:@"Enter a number"];

  [self.formatterResult setUsesGroupingSeparator:YES];

  // self.formatterResult.usesGroupingSeparator = YES;
  // FIXME: finish the function formatter implementation

//  self.handler = [NSDecimalNumberHandler
//      decimalNumberHandlerWithRoundingMode:NSRoundPlain
//                                     scale:NSDecimalNoScale
//                          raiseOnExactness:NO
//                           raiseOnOverflow:NO
//                          raiseOnUnderflow:NO
//                       raiseOnDivideByZero:NO];
  
  self.handler = [NSDecimalNumberHandler
                  decimalNumberHandlerWithRoundingMode:NSRoundPlain
                  scale:NSDecimalNoScale
                  raiseOnExactness:NO
                  raiseOnOverflow:NO
                  raiseOnUnderflow:NO
                  raiseOnDivideByZero:NO];
                 
  self.rangeFirstNumber = NSMakeRange(0, 1);
  self.rangeSecondNumber = NSMakeRange(1, 1);
  self.currentInputFieldTextColor = self.InputField.textColor;
  self.currentInputFieldbackgroundColor= self.InputField.backgroundColor;
}

#pragma mark FUTURE : IMPLEMENT maybe a single function checkInput for null
// 1 sign and minus - register in BOOL - can be more effective

// follow the input - register initial states, calculate number of digits. When
// > 1 - no need to check for single minus or null

// Only perform full (string) check on new numbers

#pragma mark FUTURE : IMPLEMENT tags instead of isEqualToString check
// ns_enums for states
#pragma mark -
- (IBAction)numberEntered:(UIButton *)number {
#pragma mark FUTURE : why cannot we put + / -as a separate operation

  if ([number.currentTitle isEqualToString:@"+ / -"]) {
    if (self.inDefaultState || self.inputFieldHasNull) {
      [self.stringWithoutSeparators setString:self.minusString];
      self.InputField.text = self.minusString;
      self.isNegative = YES;
      self.inputFieldHasAMinus = YES;
      self.inDefaultState = NO;
      self.inputFieldHasNull = NO;
    } else if (self.inputFieldHasAMinus) {
      [self clearScreen];
      self.inputFieldHasAMinus = NO;
    }
#pragma mark FUTURE : minus can also be a secondOperand currently,                in future must fix it

    else if (self.operationEntered && !self.secondOperandEntered) {
      [self.stringWithoutSeparators setString:self.minusString];
      self.InputField.text = self.minusString;
      self.isNegative = YES;

      // switching second operand into append mode
      // TODO: can't use bare minus as an operand, check for minus in operation
      // and isNegative
      self.secondOperandEntered = YES;
      self.inDefaultState = NO;
      self.inputFieldHasAMinus = YES;
    }
#pragma mark CHECK : if we need to check inputFieldHasNull

    // else just change sign
    else if ([[self.InputField.text substringWithRange:self.rangeFirstNumber]
            isEqualToString:self.minusString]) {
      [self.stringWithoutSeparators
          deleteCharactersInRange:self.rangeFirstNumber];
      [self formattedOutput];
      self.isNegative = NO;
    } else {
      [self.stringWithoutSeparators
          setString:[NSString
                        stringWithFormat:@"-%@", self.stringWithoutSeparators]];
      self.isNegative = YES;
    }
  
#pragma mark CHECK : if it overwrites
    //[self.stringWithoutSeparators insertString:self.minusString atIndex:0];
    
    // OUTPUT NUMBER WITH MINUS
   if ( self.inputFieldHasAMinus || (self.dotPresent && self.InputField.text.length < 3)) {
      self.InputField.text = self.stringWithoutSeparators;
    } else {
      [self formattedOutput];
    }
    self.isNegative = YES;
  }
// put minus - if there is 0.0, empty, or state before entering another
// operand

// TODO: try switch  [self switchOperandToAppendMode]; and combine the
// checkwith before empty state

// FIXME: probably not coming this far,
//    else if (!self.firstOperandEntered) {
//      // switching first operand into append mode
//      // self.InputField.text = self.minusString;
//      [self.stringWithoutSeparators setString:self.minusString];
//      self.InputField.text = self.stringWithoutSeparators;
//      self.firstOperandEntered = YES;
//      self.isNegative = YES;
//    }

// NOT COMING HERE
// Alternative to BOOL register:
// NSRange range = [self.InputField.text rangeOfString:self.minusString];
// if(range.location == NSNotFound){
// self.InputField.text= [self.minusString
// stringByAppendingString:self.InputField.text];}

//    NSDecimalNumber * num = [ [self stringToDecimal:self.InputField.text]
//    decimalNumberByMultiplyingBy:
//         [NSDecimalNumber decimalNumberWithMantissa: 1
//                                           exponent: 0
//                                         isNegative: YES]];
//    self.InputField.text = [num stringValue];

// Alternative

// unsigned long num =[[self
// stringToDecimal:self.InputField.text]unsignedLongValue];
// self.InputField.text = [NSString stringWithFormat:@"%lu",num];

#pragma mark FUTURE : output symbols but mean numbers,                                or parse later symbols to get numbers
// round them?
#pragma mark FUTURE : IMPLEMENT e

// Currently no need to check
// to  [self.stringWithoutSeparators isEqualToString:@"0"] ||
// self.specialNumberPresent ||

#pragma mark CHECK : that no formattedOutput is present in the dotPresent part

  // switching type of append mode, if dotPresent don't use formattedOutput,
  // we don't want to add / change Grouping Separators after the dot has beem
  // added

  // IF THE NUMBER IS NOT + -
  else if (self.dotPresent) {
// check number of digits, null, minus

#pragma mark if the button is 0
    // getting 0 for 0
    if ([number.currentTitle isEqualToString:@"0"]) {
// WAS if(self.dotPresent && self.InputField.text.length < 3) {
#pragma mark CHECK if we need it
//    if (self.InputField.text.length < 3) {
// WAS   self.InputField.text = self.nullDotString;
// WAS  [self.stringWithoutSeparators setString: self.nullDotString];
#pragma mark CHECK : this before processing(operation) or                              make a normal switch to append mode
//          [self switchOperandToAppendMode];
//          [self.stringWithoutSeparators appendString:self.nullString];
//          self.InputField.text = self.nullString;
// here we get just 0  [self formattedOutput];
//          self.inDefaultState = NO;
//   }
#pragma mark CHECK : if we need to check inputFieldHasNull,
      // maybe put it in the beginning
      // WAS if (self.inputFieldHasNull || self.InputField.text.length == 0)
      //      if (self.inputFieldHasNull || self.inDefaultState) {
      //        // TODO: Null can be input, but can't append to null !!!
      //        // TODO: when appending need to check a single null
      //        //self.InputField.text = self.nullString;
      //      //  [self.stringWithoutSeparators setString:self.nullString];
      //        self.inputFieldHasNull = YES;
      //        self.inDefaultState = NO;
      //        self.inputFieldHasAMinus = YES;
      //      } else if(self.inputFieldHasAMinus){
      //        //ignore
      //      }

      if (self.operationEntered && !self.secondOperandEntered) {
        self.InputField.text = self.nullString;
        [self.stringWithoutSeparators setString:self.nullString];
        self.inputFieldHasNull = YES;
        self.inDefaultState = NO;
        self.secondOperandEntered = YES;
      }

      //  else if(self.isNegative && self.InputField.text.length == 1)
      //    {
      //    self.InputField.text = self.minusNulNullString;
      //    [self.stringWithoutSeparators setString: self.minusNulNullString];
      //    self.inputFieldHasMinusNullNull = YES;
      //    self.inputFieldHasNullNull = NO;
      //    }
      else {
        //  TODO:compose this string from pre- and post-dot parts
        // OR simply append to both parts
        //    self.inDefaultState
        [self.stringWithoutSeparators appendString:self.nullString];
        self.InputField.text = [NSString
            stringWithFormat:@"%@%@", self.InputField.text, self.nullString];
        self.inDefaultState = NO;
      }
    }

    // if the button is not "0"
    else if (self.operationEntered) {
      if (self.secondOperandEntered) {
        [self.stringWithoutSeparators appendString:number.currentTitle];
        self.InputField.text = self.stringWithoutSeparators;
      } else if (!self.secondOperandEntered) {
        //   [self getInputToScreen:number];
        self.InputField.text = number.currentTitle;
        [self.stringWithoutSeparators setString:number.currentTitle];
        // switching second operand into append mode
        self.secondOperandEntered = YES;
      }
       self.inputFieldHasAMinus = NO;
    } 
    
    else if (!self.operationEntered) {
      if (!self.firstOperandEntered) {
        // No operands - Entering first operand
        [self.stringWithoutSeparators appendString:number.currentTitle];
        self.InputField.text = self.stringWithoutSeparators;
        // WAS    self.InputField.text = number.currentTitle;
        self.firstOperandEntered = YES;
      }
      // Still entering first operand
      else if (self.firstOperandEntered) {
        [self.stringWithoutSeparators appendString:number.currentTitle];
        self.InputField.text = self.stringWithoutSeparators;
      }
    }
     self.inputFieldHasAMinus = NO;
  }

  else if (!self.dotPresent) {
// check number of digits, null, minus

#pragma mark if the button is 0
    // getting 0 for 0
    if ([number.currentTitle isEqualToString:@"0"]) {
// WAS if(self.dotPresent && self.InputField.text.length < 3) {

//        if (self.InputField.text.length < 3) {
//// WAS   self.InputField.text = self.nullDotString;
//// WAS   [self.stringWithoutSeparators setString: self.nullDotString];
//#pragma mark CHECK : this before processing(operation) or                             \
//                                     make a normal switch to append mode
//          [self switchOperandToAppendMode];
//          [self.stringWithoutSeparators appendString:number.currentTitle];
//          self.InputField.text = self.stringWithoutSeparators;
//          // here we get just 0  [self formattedOutput];
//          self.inputFieldHasNull = NO;
//        }

#pragma mark CHECK : if we need to check inputFieldHasNull,
      // maybe put it in the beginning
      // WAS if (self.inputFieldHasNull || self.InputField.text.length == 0)
      if (self.inputFieldHasNull || self.inDefaultState) {
        // TODO: Null can be input, but can't append to null !!!
        // TODO: when appending need to check a single null
        self.InputField.text = self.nullString;
        [self.stringWithoutSeparators setString:self.nullString];
      } else if (self.inputFieldHasAMinus) {
        // ignore
      } else if (self.operationEntered && !self.secondOperandEntered) {
        self.InputField.text = self.nullString;
        [self.stringWithoutSeparators setString:self.nullString];
        self.secondOperandEntered = YES;
      }

      //  else if(self.isNegative && self.InputField.text.length == 1)
      //    {
      //    self.InputField.text = self.minusNulNullString;
      //    [self.stringWithoutSeparators setString: self.minusNulNullString];
      //    self.inputFieldHasMinusNullNull = YES;
      //    self.inputFieldHasNullNull = NO;
      //    }
      else {
        //  [self.stringWithoutSeparators appendString:number.currentTitle];
        //  self.InputField.text = self.stringWithoutSeparators;
        [self formattedOutput];
      }
       self.inputFieldHasNull = YES;
    }
    // if the button is not "0"
    else if (self.operationEntered) {
      if (self.secondOperandEntered) {
        [self.stringWithoutSeparators appendString:number.currentTitle];
        [self formattedOutput];
      } else if (!self.secondOperandEntered) {
        //   [self getInputToScreen:number];
        self.InputField.text = number.currentTitle;
        [self.stringWithoutSeparators setString:number.currentTitle];
        // switching second operand into append mode
        self.secondOperandEntered = YES;
      }
    }  else if (self.inputFieldHasAMinus) {
      [self.stringWithoutSeparators appendString:number.currentTitle];
      [self formattedOutput];
       [self switchOperandToAppendMode];
    }
    
    else if (!self.operationEntered) {
      if (!self.firstOperandEntered) {
        // No operands - Entering first operand
        [self.stringWithoutSeparators setString:number.currentTitle];
        [self formattedOutput];
        // changed    self.InputField.text = number.currentTitle;
        self.firstOperandEntered = YES;
      }
      //  }
      // Still entering first operand
      else if (self.firstOperandEntered) {
        [self.stringWithoutSeparators appendString:number.currentTitle];
        [self formattedOutput];
      }
    }
    self.inputFieldHasAMinus = NO;
  }
self.inDefaultState = NO;

}

#pragma mark -
- (IBAction)operationEntered:(UIButton *)operation {
// if logic is implemented, minus won't be impossible here  because it's not an
// operand
#pragma mark FUTURE : remove minus check, when append mode is implemented
  if (self.InputField.text.length > 0 &&
      ![self.InputField.text isEqualToString:self.minusString])
// WAS if([self.InputField.text length] > 0 && ![self.InputField.text
// isEqualToString:(self.minusString)])

#pragma mark CHECK : for minus in operation and isNegative, can't use bare minus as an operand 
  {
    if (!self.secondOperandEntered) {
      self.firstOperand =
          [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                            locale:self.locale];
    }

    else if (self.secondOperandEntered) {
      [self evaluateAndShowResult];

      // save the result in case it will be used for the next operation
      self.firstOperand =
          [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                            locale:self.locale];
      self.secondOperandEntered = NO;

#pragma mark TODO: possibly can save operand for equalsEntered now (evaluation without further input)
    }
  }
  self.firstOperandEntered = YES;
  self.currentOperation = operation.currentTitle;
  self.operationEntered = YES;
  // resetting dot for the next operand
  self.dotPresent = NO;
  self.isNegative = NO;
  self.savedOperandPresent = NO;
  self.savedOperationPresent = NO;
}

#pragma mark -
- (IBAction)equalsEntered:(UIButton *)button {
#pragma mark FUTURE : remove 2 minus checks here, when append mode is implemented
#pragma mark CHECK : if additional operand is present and take
// checking if isNegative was the last operation
// in this case, self.operationEntered should be NO

// next isNegative takes the result on the screen as first operand

// need operation and input
#pragma mark CHECK : how it is possible that we have second operand here after evaluation
  // WAS   self.currentOperation && self.stringWithoutSeparators.length > 0 &&

  // self.secondOperand &&
  // WAS !self.inDefaultState &&
  if ( !self.inputFieldHasAMinus) {
// WAS ![self.stringWithoutSeparators isEqualToString:self.minusString]

#pragma mark operation with a previous result
//WAS !self.firstOperandEntered &&
    if ( !self.operationEntered && !(self.savedOperandPresent &&
                                    self.savedOperationPresent)) {
      // ignore
    } else if (!self.operationEntered && self.savedOperandPresent &&
               self.savedOperationPresent) {
      self.firstOperand = self.savedOperand;
      self.firstOperandEntered = YES;

      self.currentOperation = self.savedOperation;
      self.savedOperationPresent = YES;
      self.operationEntered = YES;
         [self evaluateImmediatelyCurrentOperation];
    } else {
//   else if (self.operationEntered) {
//     self.secondOperand =
//     [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators];
//     self.secondOperandEntered = YES;
//     [self evaluateAndShowResult];
//   }

#pragma mark CHECK : if locale needed
    self.secondOperand =
        [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                          locale:self.locale];

    self.secondOperandEntered = YES;
    [self evaluateAndShowResult];
 }
 
    self.savedOperand = self.firstOperand;
    self.savedOperation = self.currentOperation;
    self.savedOperationPresent = YES;
    self.savedOperandPresent = YES;
}
}


#pragma mark -
- (IBAction)evaluateImmediately:(UIButton *)operation {
  //  TODO:!LOGIC
  //  WHILE (not minus){
  //  if null or minus null - evaluate as null - in certain operations
  //  if user pressed immediateOperation instead of equals -> evaluate previous
  //  operation and use the result for immediateOperation
  //  if !self.secondOperandEnetered use firstOperand
  //  else use the valid number in the input field
  //  }

  // TODO: can't use this check for tan, cos, sin
  // TODO: maybe we need to check self.stringWithoutSeparators
  if (!self.inputFieldHasAMinus
      // WAS || ![self.InputField.text isEqualToString:self.minusString]
      ) {
    // if(!self.inDefaultState && !self.inputFieldHasNull &&
    //        ![self.InputField.text isEqualToString:self.minusString]) {

    if (self.secondOperandEntered) {
// evaluating previous operation and using the result as an operand

#pragma mark FUTURE : remove minus check, when append mode is implemented

// checking for self.secondOperandEntered to exclude default null, so null can
// be entered and used as an operand

#pragma mark CHECK : if self.secondOperandEntered maybe excludes isEqualToString : ( self.minusString)

#pragma mark operation with a previous result
#pragma mark CHECK : if locale needed
      self.secondOperand =
          [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                            locale:self.locale];
      [self evaluateAndShowResult];

// Using the result further
#pragma mark CHECK : if evaluateAndShowResult does reset self.firstOperandEntered = NO;
    }

    else if ((!self.firstOperandEntered || self.inDefaultState) &&
             self.InputField.text.length > 0) {
      self.firstOperand =
          [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                            locale:self.locale];
      self.firstOperandEntered = YES;
    }
// if we have no second operand, perform immediate operation (overriding the
// previous one) on the first operand, and resetting
#pragma mark CHECK : if the reset self.operationEntered = NO;                        can be done here(compare to reset after normal evaluation)

    //    else if(!self.secondOperandEntered) {
    //      // we have no second operand, overriding previous operation -
    //      perform
    //      // operation on the first operand
    //      self.operationEntered = NO;
    //    }

    // continuing with operation
    self.currentOperation = operation.currentTitle;
    self.operationEntered = YES;

    // resetting dot for the next operand
    // self.dotPresent = NO;
    // self.isNegative = NO;
    [self evaluateImmediatelyCurrentOperation];
}
}

  - (void)evaluateImmediatelyCurrentOperation {
    if (self.stringWithoutSeparators.length > 0) {
      self.firstOperand =
      [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                        locale:self.locale];
      self.firstOperandEntered = YES;
    }
    
    if ([self.currentOperation isEqualToString:@"^2"]) {
      
      // @try {
      self.operationResult = [self.firstOperand decimalNumberByRaisingToPower:2
                                                                 withBehavior:self.handler];
      
      //        NSComparisonResult result = NSDecimalCompare((__bridge const NSDecimal * _Nonnull)(self.result), NSDecimalMaxSize);
      //
      //        if (result == NSOrderedAscending) {
      //          NSLog(@"self.result is less than NSDecimalMaxSize");
      //        }
      //        else if (result == NSOrderedDescending) {
      //          NSLog(@"self.result is more than NSDecimalMaxSize");
      //        }
      //       else if (result == NSOrderedSame) {
      //        NSLog(@"self.result is the same as NSDecimalMaxSize");
      //            }
      
      
      
      //        if ([self.operationResult compare:decimalMax] == NSOrderedAscending) {
      //          NSLog(@"self.result is less than %g",[decimalMax doubleValue]);
      //        }
      
      // self.InputField.text = [NSString stringWithFormat:@"%@", NSDecimalNumberOverflowException.description];
      
      //  self.InputField.text = NSDecimalNumberOverflowException
      
      //withBehavior:[NSDecimalNumberHandler defaultDecimalNumberHandler]];
      
      //      } @catch (NSException *exc) {
      //        // if exc.description isEqualToString NSDecimalNumberOverflowException
      //        //  NSCalculationError error = [self getDecimalError]; 
      //        self.InputField.text = [NSString stringWithFormat:@"%@", exc.reason];
      
      // BUY A PREMIUM VERSION
    } 
    //      @finally {
    //        NSLog([NSString stringWithFormat:@"%@", NSDecimalNumberOverflowException.description];) }
    
    else if ([self.currentOperation isEqualToString:@"^3"]) {
      self.doubleResult = pow([self.firstOperand doubleValue], 3);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"1/n"]) {
      self.doubleResult = pow([self.firstOperand doubleValue], -1);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"sqrt"]) {
      if (self.firstOperand > 0) {
        self.doubleResult = sqrt([self.firstOperand doubleValue]);
        self.doubleResultPresent = YES;
        
        // self.Result = [NSDecimalNumber
        // decimalNumberWithMantissa:self.doubleResult exponent:0
        // isNegative:NO];
        
        //  long double SquareRoot = sqrtl([self.FirstOperand longLongValue]);
        // self.InputField.text = [NSString stringWithFormat:@"%Lf",
        // SquareRoot];
        //          [self resetAll];
      } else {
        
        if ([self.firstOperand compare: [NSDecimalNumber zero]] == NSOrderedAscending)
          {
          self.InputField.text = [NSString stringWithFormat:@"%@. Please enter positive number!",[NSDecimalNumber notANumber]];
          } 
        
        // self.InputField.text = @"Please enter positive number";
        return;
      }
    }
    
    // cubic root
    else if ([self.currentOperation isEqualToString:@"cbrt"]) {
      if (self.firstOperand > 0) {
        self.doubleResult = cbrt([self.firstOperand doubleValue]);
        self.doubleResultPresent = YES;
      } else {
        self.InputField.text = @"Please enter positive number";
        return;
      }
    }
    
    else if ([self.currentOperation isEqualToString:@"|x|"]) {
      self.doubleResult = fabs([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"exp"]) {
      self.doubleResult = exp([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"log"]) {
      self.doubleResult = log([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"log10"]) {
      self.doubleResult = log10([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"ln"]) {
      // TODO: ln
      //   self.doubleResult = ln ([self.FirstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"sin"]) {
      self.doubleResult = sin([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"asin"]) {
      self.doubleResult = asin([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"acos"]) {
      self.doubleResult = acos([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"cos"]) {
      self.doubleResult = cos([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"tan"]) {
      self.doubleResult = tan([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    
    else if ([self.currentOperation isEqualToString:@"atan"]) {
      self.doubleResult = atan([self.firstOperand doubleValue]);
      self.doubleResultPresent = YES;
    }
    else {
    //TODO: move [self evaluateAndShowResult] to equalsEntered, otherwise it's doing double work 
    // 
    [self evaluateAndShowResult];
    }
  
  if ([self.operationResult compare:[NSDecimalNumber notANumber]] == NSOrderedSame) {
    //NSLog(@"Maximum value of this calculator is reached: %g",[[NSDecimalNumber maximumDecimalNumber] doubleValue]);
    self.InputField.textColor = [UIColor redColor];
    self.InputField.textAlignment = NSTextAlignmentLeft;
    // self.InputField.adjustsFontSizeToFitWidth = YES;
    self.InputField.numberOfLines = 2;
    self.InputField.text = [NSString stringWithFormat:@"Error: maximum number is reached: %g",[[NSDecimalNumber maximumDecimalNumber] doubleValue]];
    
    //self.InputField.textColor = [UIColor whiteColor];
    //self.InputField.backgroundColor = [UIColor colorWithRed:0.0 green:0.1 blue:0.2 alpha:1.0];
    //self.InputField.textColor = [UIColor colorWithRed:0.0 green:0.1 blue:0.2 alpha:1.0];
    
  } 
  else {
    [self showResult];
    [self resetAll];
    // to be able to evaluate again the same operation by pressing equals again
    self.savedOperand =
    [NSDecimalNumber decimalNumberWithString:self.stringWithoutSeparators
                                      locale:self.locale];
    self.savedOperation = self.currentOperation;
    self.savedOperandPresent = YES;
    self.savedOperationPresent = YES;
  }
}
  
- (void)evaluateAndShowResult {
  if ([self.currentOperation isEqualToString:@"+"]) {
    self.operationResult = [self.firstOperand decimalNumberByAdding:self.secondOperand];
  }

  else if ([self.currentOperation isEqualToString:self.minusString]) {
    self.operationResult =
        [self.firstOperand decimalNumberBySubtracting:self.secondOperand];
  }

  else if ([self.currentOperation isEqualToString:@"*"]) {
    self.operationResult =
        [self.firstOperand decimalNumberByMultiplyingBy:self.secondOperand];
  }

  else if ([self.currentOperation isEqualToString:@"^n"]) {
    self.doubleResult =
        pow([self.firstOperand doubleValue], [self.secondOperand doubleValue]);
    self.doubleResultPresent = YES;
  }
#pragma mark FUTURE:
  //
  // dividingbyzero returns    HUGE_VAL -- positive infinity  ??
  //

  else if ([self.currentOperation isEqualToString:@"/"]) {
   
     if ([self.secondOperand compare:[NSDecimalNumber zero]] == NSOrderedSame) {
      
   // if (self.secondOperand == [NSDecimalNumber zero]) {
      //self.InputField.text = @"Cannot divide by zero";
      self.InputField.text = [NSString stringWithFormat:@"%f", HUGE_VAL];
      //                    //red or other colour text
      return;
    }

    else if (self.secondOperand > 0 || self.secondOperand < 0) {
      self.operationResult =
          [self.firstOperand decimalNumberByDividingBy:self.secondOperand];
    }
  }

  else if ([self.currentOperation isEqualToString:@"mod"]) {
    // fmod (modulus % function), remainder
  }

  [self showResult];
  [self resetAll];
}

#pragma mark -
#pragma mark CHECK:
- (IBAction)dotEntered:(UIButton *)button {
  if (!self.dotPresent) {
    //    if (self.inputFieldHasNullNull )
    //      {
    //      //skip
    //      }
    //
    //    else if (self.inputFieldHasMinusNullNull )
    //      {
    //      //skip
    //      }
    //
    // if([self.InputField.text length] == 0)
    //
    //{
    // if number is present add dot to it
    if ([self.InputField.text isEqualToString:@"-"])
#pragma mark CHECK : if (self.isNegative &&self.InputField.text.length < 3)
    {
      [self.stringWithoutSeparators setString:@"-0."];
      self.InputField.text = self.stringWithoutSeparators;
      self.dotPresent = YES;
      //    self.isNegative = NO;
      // self.inputFieldHasNull = NO;
    }
#pragma mark CHECK : if we need to check inputFieldHasNull
    // WAS  else if (self.inputFieldHasNull || self.InputField.text.length == 0
    // ||
    else if (self.inputFieldHasNull || self.inDefaultState ||
             (!self.firstOperandEntered && !self.operationEntered) ||
             (self.operationEntered && !self.secondOperandEntered)) {
      [self.stringWithoutSeparators setString:@"0."];
      self.InputField.text = self.stringWithoutSeparators;
      //    self.isNegative = NO;

      // FIXME: when taking next result, the number present is not cleared
      //   (self.operationEntered && self.InputField.text > 0)

      // assume 0 if number is not entered yet, or if the state has been reset
      [self switchOperandToAppendMode];

      // WAS   if ((!self.operationEntered)&& (!self.firstOperandEntered))
      //      {
      //      self.firstOperandEntered = YES;
      //      }
      //
      //    else if ((self.operationEntered)&& (!self.secondOperandEntered))
      //      {
      //      self.secondOperandEntered = YES;
      //      }
    }

    // else append
    else {
      [self.stringWithoutSeparators appendString:self.decimalSeparator];
      self.InputField.text = self.stringWithoutSeparators;
    }
    self.dotPresent = YES;
    self.inputFieldHasNull = NO;
    self.inDefaultState = NO;
  }
}

#pragma mark -
// pure formatted output, no logic
- (void)showResult {
  //[self.formatter setMaximumFractionDigits:8];
  if (self.doubleResultPresent) {
    @try {
      self.InputField.text = [NSNumberFormatter
          localizedStringFromNumber:[NSNumber
                                        numberWithDouble:self.doubleResult]
                        numberStyle:NSNumberFormatterDecimalStyle];

      [self.stringWithoutSeparators
          setString:
              [NSNumberFormatter
                  localizedStringFromNumber:
                      [NSNumber numberWithDouble:self.doubleResult]
                                numberStyle:NSNumberFormatterDecimalStyle]];

      self.InputField.text = self.stringWithoutSeparators;

      // WAS self.InputField.text = [NSString stringWithFormat:@"%g",
      // self.doubleResult];
      // WAS  [self.stringWithoutSeparators setString:[NSString
      // stringWithFormat:@"%f", self.doubleResult]];
      self.doubleResultPresent = NO;
      // TODO: do we need to reset here?
    } @catch (NSException *exc) {
      @throw(NSDecimalNumberOverflowException);
      self.InputField.text = [NSString stringWithFormat:@"%@", exc.description];
    }
  } else {
    [self.stringWithoutSeparators setString:[self.operationResult stringValue]];
    //[self.stringWithoutSeparators setString:[NSString stringWithFormat:@"%f",
    //[self.Result doubleValue]]];

    if (self.stringWithoutSeparators.length > 9) {
      [self scientificStyleOutput];
      self.InputField.text =
          [NSString stringWithFormat:@"%g", [self.operationResult doubleValue]];
      //
    } else {
      // WAS self.InputField.text = [[self.formatterResult
      // numberFromString:self.stringWithoutSeparators] stringValue];
      [self formattedOutput];
    }
  }

#pragma mark FUTURE : how many digits should it show

  // scientific exponent

  // self.stringWithoutSeparators = [[self.Result stringValue] mutableCopy];

  // self.InputField.text = [self.formatter stringFromNumber:self.Result]

  //  NSNumber *num = [self.formatter numberFromString:[self.Result
  //  stringValue]];

  //   self.InputField.text = [num stringValue];

  // self.InputField.text = [self.formatter stringFromNumber:self.Result];

  // self.InputField.text = [[self.formatter numberFromString:[self.Result
  // stringValue]] stringValue];

  [self resetAll];
}

#pragma mark -
- (void)resetAll {
  self.firstOperandEntered = NO;
  self.operationEntered = NO;
  // need to save second operand for another Operation
  self.secondOperandEntered = NO;
  self.dotPresent = NO;
  //    self.specialNumberPresent = NO;
  self.isNegative = NO;
  self.inDefaultState = YES;
  self.savedOperationPresent = NO;
  self.savedOperationPresent = NO;
  self.inputFieldHasAMinus = NO;
  self.InputField.textColor = self.currentInputFieldTextColor;
  self.InputField.backgroundColor = self.currentInputFieldbackgroundColor;
  self.InputField.numberOfLines = 1;
  self.InputField.textAlignment = NSTextAlignmentRight;
}

- (IBAction)clearAll:(UIButton *)button {
  [self clearScreen];
  [self resetAll];
}

- (IBAction)removeNumber:(UIButton *)button {
#pragma mark CHECK : if we need to inspect string for null
  // WAS if(self.inputFieldHasNull ||  self.InputField.text.length == 0)
  if (self.inputFieldHasNull || self.inDefaultState) {
    // nothing to remove, just resetting states then
    self.isNegative = NO;
    self.dotPresent = NO;
  } else if (self.InputField.text.length == 1)
#pragma mark CHECK : if need to add check
  {
    // resetting states and clearing
    [self clearScreen];
    self.isNegative = NO;
    self.dotPresent = NO;

    // logic
    if (self.operationEntered) {
      self.secondOperandEntered = NO;
    } else {
      self.firstOperandEntered = NO;
    }
  }

  // else removing one digit from stringWithoutSeparators
  else
#pragma mark CHECK : if it can be pure else
  // WAS if(self.InputField.text.length > 0)
  {
    [self.stringWithoutSeparators
        deleteCharactersInRange:NSMakeRange(
                                    (self.stringWithoutSeparators.length - 1),
                                    1)];
    [self formattedOutput];
  }
}

- (void)clearScreen {
  self.InputField.text = self.nullString;
  [self.stringWithoutSeparators setString:self.nullString];
  self.inDefaultState = YES;
}

- (void)switchOperandToAppendMode {
  if (self.operationEntered) {
    self.secondOperandEntered = YES;
  } else if (!self.firstOperand) {
    self.firstOperandEntered = YES;
  }
}

- (void)formattedOutput {
  // TODO:
  // if (self.InputField.text.length < 9){
  // }

  // TODO: with formatting, added zeros are rounded, but still need grouping
  // separator
  // not working [self.formatterInput generatesDecimalNumbers];

  self.InputField.text = [NSNumberFormatter
      localizedStringFromNumber:
          [self.formatterInput numberFromString:self.stringWithoutSeparators]
                    numberStyle:NSNumberFormatterDecimalStyle];

  // self.InputField.text  = [[self.formatterInput
  // numberFromString:self.stringWithoutSeparators] stringValue];

  // return [NSDecimalNumber decimalNumberWithString:
}

- (void)scientificStyleOutput {
  // self.InputField.text = [[self.formatterScientific
  // numberFromString:self.stringWithoutSeparators] stringValue];
  //

  self.InputField.text = [NSNumberFormatter
      localizedStringFromNumber:self.operationResult
                    numberStyle:NSNumberFormatterScientificStyle];
}

#pragma mark FUTURE : IMPLEMENT percent function
- (void)percentStyleOutput {
  self.InputField.text = [NSNumberFormatter
      localizedStringFromNumber:self.operationResult
                    numberStyle:NSNumberFormatterPercentStyle];
  // self.inputFieldHasNull = YES;
}

-(void)checkOperationResultPrecision{
  if ([self.operationResult compare: [NSNumber numberWithLongLong:NSDecimalMaxSize]] == NSOrderedDescending) {
    NSLog(@"Precision is lost with numbers more than: %d",NSDecimalMaxSize);
 } 
 }
                                                           
//- (NSCalculationError)getDecimalError {
//  NSCalculationError decimalError = NSDecimalDivide(
//      (__bridge NSDecimal * _Nonnull)(self.result),
//      (__bridge const NSDecimal *_Nonnull)(self.firstOperand),
//      (__bridge const NSDecimal *_Nonnull)(self.secondOperand), NSRoundPlain);
//  switch (decimalError) {
//  case NSCalculationNoError:
//    NSLog(@"Operation successful");
//  case NSCalculationLossOfPrecision:
//    NSLog(@"Error: Operation resulted in loss of precision");
//  case NSCalculationUnderflow:
//    NSLog(@"Error: Operation resulted in underflow");
//  case NSCalculationOverflow:
//    NSLog(@"Error: Operation resulted in overflow");
//  case NSCalculationDivideByZero:
//    NSLog(@"Error: Tried to divide by zero");
//  default:
//    NSLog(@"Unknown error");
//  }
//  return decimalError;
//}

//- (BOOL)checkInputForNegative {
//  if ([[self.InputField.text substringWithRange:self.rangeFirstNumber]
//          isEqualToString:self.minusString]) {
//    self.isNegative = YES;
//    return YES;
//  } else {
//    self.isNegative = NO;
//    return NO;
//  }
//}

//- (NSDecimalNumber *) stringToDecimal: (NSString *) string {
//    return [NSDecimalNumber decimalNumberWithString:string];
//}

@end
