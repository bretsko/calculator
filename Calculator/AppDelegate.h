//
//  AppDelegate.h
//  Calculator
//
//  Created by Admin on 10/5/15.
//  Copyright (c) 2015 Softserve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


//1. for Ipad (regular x regular) - change main vertical constraint and font size, add operations. 


//6. double exp(double x -- Compute exponential of x

//
//5. dividingbyzero returns    HUGE_VAL -- positive infinity.  ??
//
//double sin(double x) -- Compute sine of angle in radians.
//double acos(double x) -- Compute arc cosine of x.
//double asin(double x) -- Compute arc sine of x.
//double atan(double x) -- Compute arc tangent of x.
//double cos(double x) -- Compute cosine of angle in radians.
//double tan(double x) -- Compute tangent of angle in radians.
//
//7. add fmod (modulus % function), remainder
//
//double ceil ( double ) – if the argument has any decimal part, returns the next bigger integer
//NSLog(@”res: %.f”, ceil(3.000000000001)); //result 4
//NSLog(@”res: %.f”, ceil(3.00)); //result 3
//
//double floor ( double ) – removes the decimal part of the argument
//NSLog(@”res: %.f”, floor(3.000000000001)); //result 3
//NSLog(@”res: %.f”, floor(3.9999999)); //result 3
//
//double round ( double ) – rounds the argument
//NSLog(@”res: %.f”, round(3.5)); //result 4
//NSLog(@”res: %.f”, round(3.46)); //result 3
//NSLog(@”res: %.f”, round(-3.5)); //NB: this one returns -4
//
//
//6. CHECK cbrt (cubic root)
//
//8. CHECK
//double pow (double x, double y) -- Compute x raised to the power y.
//double pow ( double, double ) – power of
//NSLog(@”%.f”, pow(3,2) ); //result 9
//NSLog(@”%.f”, pow(3,3) ); //result 27
//
//double sqrt(double x) -- Compute the square root of x.
//double  sqrt( double ) – square root
//NSLog(@”%.f”, sqrt(16) ); //result 4
//NSLog(@”%.f”, sqrt(81) ); //result 9
//
//double fabs (double x ) -- Compute absolute value of x.
//
//add 100'432'324'232  -  higher comma
//
//
//
//div_t div(int number, int denom) -- Divide one integer by another.
//
//double floor(double x) -- Get largest integral value less than x.
//double fmod(double x, double y) -- Divide x by y with integral quotient and return remainder.
//double frexp(double x, int *expptr) -- Breaks down x into mantissa and exponent of no.
//
//labs(long n) -- Find absolute value of long integer n.
//
//double ldexp(double x, int exp) -- Reconstructs x out of mantissa and exponent of two.
//ldiv_t ldiv(long number, long denom) -- Divide one long integer by another.
//double modf(double x, double *intptr) -- Breaks x into fractional and integer parts.
//
//void srand(unsigned seed) -- Set a new seed for the random number generator (rand).
//
//
//
//OPTIONAL:
//
//add function memory (M+)
//
//make custom images for buttons, appicon in Inkscape
//
//Try ns_enum (type, name){} to use with switch (states) - operation
//
//Try implement tags
//
//Put functionality in a business object or define a protocol for functionality
//
//Add notifications with alert (for dividing by zero or other) To use an alert in your code, you create a UIAlertController and specify the UIAlertControllerStyleAlert.
//
//
//Try Swift and BDD (start with test
//define formatter as +class method (Class method are used to initialize static variables (counter) and alloc)
//

//
//initialize with initWith method
//
//
//isfinite
//    Is finite value (macro )
//
//isinf
//    Is infinity (macro/function )


@end

